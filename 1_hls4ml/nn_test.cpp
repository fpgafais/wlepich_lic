//
//    rfnoc-hls-neuralnet: Vivado HLS code for neural-net building blocks
//
//    Copyright (C) 2017 EJ Kreinar
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include <fstream>
#include <iostream>
#include <algorithm>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <bitset>
#include <ap_int.h>

#include "firmware/nn.h"

#define CHECKPOINT 100

int main(int argc, char **argv)
{
	//load input data from text file
	std::ifstream fin("tb_data/uint8_black.dat");
	//load predictions from text file
	std::ifstream fpr("tb_data/classes.dat");


#ifdef RTL_SIM
	std::string RESULTS_LOG = "tb_data/rtl_cosim_results.log";
#else
	std::string RESULTS_LOG = "tb_data/csim_results.log";
#endif
	std::ofstream fout(RESULTS_LOG);

	std::string iline;
	std::string pline;
	int e = 0;
	int correct = 0;

	if (fin.is_open() && fpr.is_open()) {
		while ( std::getline(fin,iline) && std::getline (fpr,pline)) {

			if (e % CHECKPOINT == 0) std::cout << "Processing input " << e << std::endl;
			e++;
			// processing input data
			char* cstr=const_cast<char*>(iline.c_str());
			char* current;
			std::vector<c_input_t> in;
			current=strtok(cstr," ");
			while(current!=NULL) {
				in.push_back(atof(current));
				current=strtok(NULL," ");
			}
			// processing prediction
			cstr=const_cast<char*>(pline.c_str());
			unsigned short real_class;
			current=strtok(cstr," ");
			real_class = (unsigned short) atof(current);

			//hls-fpga-machine-learning insert data
			std::vector<c_input_t>::const_iterator in_begin = in.cbegin();
			std::vector<c_input_t>::const_iterator in_end;
			c_input_t input[N_INPUT_1_1];
			in_end = in_begin + (N_INPUT_1_1);
			std::copy(in_begin, in_end, input);
			in_begin = in_end;
			// original result
			c_output_t output[N_LAYER_6];

			std::fill_n(output, 10, 0.);

			for (int i=0; i<N_INPUT_1_1; i++) {
				input[i] = (c_input_t) input[i];
			}

			//hls-fpga-machine-learning insert top-level-function
			nn(input,output);

			//hls-fpga-machine-learning insert testbench-output
			for(int i = 0; i < N_LAYER_6; i++) {
				fout << output[i] << " ";
			}
			fout << std::endl;

			// get class with hightest probability
			short prediction = 0;
			double max_pred = 0.;
			for(int i=0; i<N_LAYER_6; i++) {
				if (output[i] > max_pred) {
					max_pred = output[i];
					prediction = i;
				}
			}
			if (prediction == real_class)
				correct++;

			if (e % CHECKPOINT == 0) {
				std::cout << "Class: " << real_class << std::endl;
				//hls-fpga-machine-learning insert predictions
				std::cout << "Quantized predictions: " << prediction << std::endl;
				//hls-fpga-machine-learning insert quantized
				for(int i = 0; i < N_LAYER_6; i++) {
					std::cout << output[i]  << " ";
				}
				std::cout << std::endl;
			}
		}
		double accuracy = (double) correct / (double) e;
		std::cout << "==========" << std::endl;
		std::cout << "Correct: " << correct << " for: " << e << " samples ";
		std::cout << "which gives: " << accuracy * 100 << "% accuracy" << std::endl;
		fin.close();
		fpr.close();
	} else {
		std::cout << "Unable to open files" << std::endl;
		return 1;
	}

	fout.close();
	std::cout << "INFO: Saved inference results to file: " << RESULTS_LOG << std::endl;

	return 0;
}
