#ifndef DEFINES_H_
#define DEFINES_H_

#include <complex>
#include "ap_int.h"
#include "ap_fixed.h"

//hls-fpga-machine-learning insert numbers
#define N_INPUT_1_1 784
#define N_LAYER_2 12
#define N_LAYER_4 40
#define N_LAYER_6 10

//typy IO
typedef unsigned char 	c_input_t;
typedef float			c_output_t;

//hls-fpga-machine-learning insert layer-precision
typedef ap_fixed<12, 8>	model_default_t;
typedef ap_uint<8>		mod_result_t;

typedef ap_uint<8> 			input_t;		// -> dense
typedef ap_fixed<15, 11>	layer2_t;		// dense -> relu
typedef ap_fixed<15, 11>	dense_accum_t;
typedef ap_fixed<11,  0> 	dense_weight_t;
typedef ap_fixed< 1,  1>	dense_bias_t;

typedef ap_fixed<15, 11> 	layer3_t;		// relu -> dense_1
typedef ap_fixed<12,  7> 	layer4_t;		// dense_1 -> relu_1
typedef ap_fixed<12,  7>	dense_1_accum_t;
typedef ap_fixed<10,  0> 	dense_1_weight_t;
typedef ap_fixed<10,  4> 	dense_1_bias_t;

typedef ap_fixed<12,  7> 	layer5_t;		// relu_1 -> dense_2
typedef ap_fixed<12,  5> 	layer6_t;		// dense_2 -> softmax
typedef ap_fixed<12,  5>	dense_2_accum_t;
typedef ap_fixed< 7,  0> 	dense_2_weight_t;
typedef ap_fixed< 6,  0> 	dense_2_bias_t;
typedef ap_fixed< 8,  2> 	result_t;		// softmax ->

#endif
