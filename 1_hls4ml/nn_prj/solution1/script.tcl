############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
############################################################
open_project nn_prj
set_top nn
add_files firmware/nn.cpp -cflags "-std=c++0x"
add_files -tb nn_test.cpp -cflags "-std=c++0x -Wno-unknown-pragmas"
add_files -tb tb_data -cflags "-Wno-unknown-pragmas"
add_files -tb firmware/weights -cflags "-Wno-unknown-pragmas"
open_solution "solution1"
set_part {xczu7ev-ffvc1156-2-e}
create_clock -period 5 -name default
config_array_partition -maximum_size 4096
#source "./nn_prj/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -format ip_catalog
