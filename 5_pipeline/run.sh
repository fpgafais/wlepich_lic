#!/bin/bash

w=1920
h=1080
framerate=60/1
fmt=YUY2

wnn=28
hnn=28

wout=224
hout=224

src="usbcam"

# SDx hardware accelerator
accel=sdxnet

sink=hdmi

# zynqmp_disp (DP)
#   + plane 34: YUY2, UYVY
# xlnx_mixer (HDMI)
#   + plane 29: YUY2
#   + plane 30: YUY2
#   + plane 31: UYVY
plane=30

# for output from sdxnet plugin
export GST_DEBUG="$accel:6"

gst-launch-1.0 \
	xlnxvideosrc src-type="${src}" io-mode="dmabuf" ! \
	videoconvert ! \
	videocrop top=428 bottom=428 left=848 right=848 ! \
	videoscale ! \
	"video/x-raw, width=${wnn}, height=${hnn}, format=${fmt}, framerate=${framerate}" ! \
	${accel} ! \
	videoscale ! \
	"video/x-raw, width=${wout}, height=${hout}, format=${fmt}, framerate=${framerate}" ! \
	videobox autocrop=true ! \
	"video/x-raw, width=${w}, height=${h}, format=${fmt}, framerate=${framerate}" ! \
	xlnxvideosink sink-type=${sink} plane-id=30 fullscreen-overlay=true


