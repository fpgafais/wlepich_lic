#!/bin/bash

header=nn.hpp
library=nn.a
func=nn
ip=ip/component.xml
clock=13.333


sdx_pack -header $header -lib lib$library \
	-func $func -map input=s_axi_AXILiteS:in:1024 \
	         -map output=s_axi_AXILiteS:out:2048 \
	-func-end \
	-ip $ip \
	-control ap_ctrl_hs=s_axi_AXILiteS:0 \
	-primary-clk ap_clk=$clock \
	-target-family zynquplus \
	-target-cpu cortex-a53 \
	-target-os linux \

