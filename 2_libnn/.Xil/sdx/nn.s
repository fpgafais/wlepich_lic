; ModuleID = '/home/wojtek/Dev/odtworzenie-licencjatu-projekt/proba1/2_libnn/.Xil/sdx/_sds/.pp/nn.ii'
source_filename = "/home/wojtek/Dev/odtworzenie-licencjatu-projekt/proba1/2_libnn/.Xil/sdx/_sds/.pp/nn.ii"
target datalayout = "e-m:e-i8:8:32-i16:16:32-i64:64-i128:128-n32:64-S128"
target triple = "aarch64--linux-gnu"

; Function Attrs: nounwind
define void @_Z2nnPhPf(i8*, float*) #0 !dbg !6 !xidane.fname !15 !xidane.function_declaration_type !16 !xidane.function_declaration_filename !17 {
  %3 = alloca i8*, align 8
  %4 = alloca float*, align 8
  store i8* %0, i8** %3, align 8
  call void @llvm.dbg.declare(metadata i8** %3, metadata !18, metadata !19), !dbg !20
  store float* %1, float** %4, align 8
  call void @llvm.dbg.declare(metadata float** %4, metadata !21, metadata !19), !dbg !22
  ret void, !dbg !23
}

; Function Attrs: nounwind readnone
declare void @llvm.dbg.declare(metadata, metadata, metadata) #1

attributes #0 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="cortex-a53" "target-features"="+crc,+crypto,+neon" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone }

!llvm.dbg.cu = !{!0}
!llvm.module.flags = !{!3, !4}
!llvm.ident = !{!5}

!0 = distinct !DICompileUnit(language: DW_LANG_C_plus_plus, file: !1, producer: "clang version 3.9.0 (tags/RELEASE_390/final)", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !2)
!1 = !DIFile(filename: "/home/wojtek/Dev/odtworzenie-licencjatu-projekt/proba1/2_libnn/.Xil/sdx/_sds/.pp/nn.ii", directory: "/home/wojtek/Dev/odtworzenie-licencjatu-projekt/proba1/2_libnn/.Xil/sdx")
!2 = !{}
!3 = !{i32 2, !"Dwarf Version", i32 4}
!4 = !{i32 2, !"Debug Info Version", i32 3}
!5 = !{!"clang version 3.9.0 (tags/RELEASE_390/final)"}
!6 = distinct !DISubprogram(name: "nn", linkageName: "_Z2nnPhPf", scope: !1, file: !1, line: 8, type: !7, isLocal: false, isDefinition: true, scopeLine: 8, flags: DIFlagPrototyped, isOptimized: false, unit: !0, variables: !2)
!7 = !DISubroutineType(types: !8)
!8 = !{null, !9, !12}
!9 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !10, size: 64, align: 64)
!10 = !DIDerivedType(tag: DW_TAG_typedef, name: "c_input_t", file: !1, line: 1, baseType: !11)
!11 = !DIBasicType(name: "unsigned char", size: 8, align: 8, encoding: DW_ATE_unsigned_char)
!12 = !DIDerivedType(tag: DW_TAG_pointer_type, baseType: !13, size: 64, align: 64)
!13 = !DIDerivedType(tag: DW_TAG_typedef, name: "c_output_t", file: !1, line: 2, baseType: !14)
!14 = !DIBasicType(name: "float", size: 32, align: 32, encoding: DW_ATE_float)
!15 = !{!"nn"}
!16 = !{!"void.c_input_t [784].1.c_output_t [10].1"}
!17 = !{!"/home/wojtek/Dev/odtworzenie-licencjatu-projekt/proba1/2_libnn/.Xil/sdx/_sds/.pp/nn.ii"}
!18 = !DILocalVariable(name: "input", arg: 1, scope: !6, file: !1, line: 8, type: !9)
!19 = !DIExpression()
!20 = !DILocation(line: 8, column: 15, scope: !6)
!21 = !DILocalVariable(name: "output", arg: 2, scope: !6, file: !1, line: 8, type: !12)
!22 = !DILocation(line: 8, column: 38, scope: !6)
!23 = !DILocation(line: 10, column: 1, scope: !6)
