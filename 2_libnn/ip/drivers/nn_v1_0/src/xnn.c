// ==============================================================
// File generated on Sun Aug 09 20:43:33 CEST 2020
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:36:41 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xnn.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XNn_CfgInitialize(XNn *InstancePtr, XNn_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Axilites_BaseAddress = ConfigPtr->Axilites_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XNn_Start(XNn *InstancePtr) {
    u32 Data;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XNn_ReadReg(InstancePtr->Axilites_BaseAddress, XNN_AXILITES_ADDR_AP_CTRL) & 0x80;
    XNn_WriteReg(InstancePtr->Axilites_BaseAddress, XNN_AXILITES_ADDR_AP_CTRL, Data | 0x01);
}

u32 XNn_IsDone(XNn *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XNn_ReadReg(InstancePtr->Axilites_BaseAddress, XNN_AXILITES_ADDR_AP_CTRL);
    return (Data >> 1) & 0x1;
}

u32 XNn_IsIdle(XNn *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XNn_ReadReg(InstancePtr->Axilites_BaseAddress, XNN_AXILITES_ADDR_AP_CTRL);
    return (Data >> 2) & 0x1;
}

u32 XNn_IsReady(XNn *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XNn_ReadReg(InstancePtr->Axilites_BaseAddress, XNN_AXILITES_ADDR_AP_CTRL);
    // check ap_start to see if the pcore is ready for next input
    return !(Data & 0x1);
}

void XNn_EnableAutoRestart(XNn *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XNn_WriteReg(InstancePtr->Axilites_BaseAddress, XNN_AXILITES_ADDR_AP_CTRL, 0x80);
}

void XNn_DisableAutoRestart(XNn *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XNn_WriteReg(InstancePtr->Axilites_BaseAddress, XNN_AXILITES_ADDR_AP_CTRL, 0);
}

u32 XNn_Get_input_r_BaseAddress(XNn *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return (InstancePtr->Axilites_BaseAddress + XNN_AXILITES_ADDR_INPUT_R_BASE);
}

u32 XNn_Get_input_r_HighAddress(XNn *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return (InstancePtr->Axilites_BaseAddress + XNN_AXILITES_ADDR_INPUT_R_HIGH);
}

u32 XNn_Get_input_r_TotalBytes(XNn *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return (XNN_AXILITES_ADDR_INPUT_R_HIGH - XNN_AXILITES_ADDR_INPUT_R_BASE + 1);
}

u32 XNn_Get_input_r_BitWidth(XNn *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XNN_AXILITES_WIDTH_INPUT_R;
}

u32 XNn_Get_input_r_Depth(XNn *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XNN_AXILITES_DEPTH_INPUT_R;
}

u32 XNn_Write_input_r_Words(XNn *InstancePtr, int offset, int *data, int length) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr -> IsReady == XIL_COMPONENT_IS_READY);

    int i;

    if ((offset + length)*4 > (XNN_AXILITES_ADDR_INPUT_R_HIGH - XNN_AXILITES_ADDR_INPUT_R_BASE + 1))
        return 0;

    for (i = 0; i < length; i++) {
        *(int *)(InstancePtr->Axilites_BaseAddress + XNN_AXILITES_ADDR_INPUT_R_BASE + (offset + i)*4) = *(data + i);
    }
    return length;
}

u32 XNn_Read_input_r_Words(XNn *InstancePtr, int offset, int *data, int length) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr -> IsReady == XIL_COMPONENT_IS_READY);

    int i;

    if ((offset + length)*4 > (XNN_AXILITES_ADDR_INPUT_R_HIGH - XNN_AXILITES_ADDR_INPUT_R_BASE + 1))
        return 0;

    for (i = 0; i < length; i++) {
        *(data + i) = *(int *)(InstancePtr->Axilites_BaseAddress + XNN_AXILITES_ADDR_INPUT_R_BASE + (offset + i)*4);
    }
    return length;
}

u32 XNn_Write_input_r_Bytes(XNn *InstancePtr, int offset, char *data, int length) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr -> IsReady == XIL_COMPONENT_IS_READY);

    int i;

    if ((offset + length) > (XNN_AXILITES_ADDR_INPUT_R_HIGH - XNN_AXILITES_ADDR_INPUT_R_BASE + 1))
        return 0;

    for (i = 0; i < length; i++) {
        *(char *)(InstancePtr->Axilites_BaseAddress + XNN_AXILITES_ADDR_INPUT_R_BASE + offset + i) = *(data + i);
    }
    return length;
}

u32 XNn_Read_input_r_Bytes(XNn *InstancePtr, int offset, char *data, int length) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr -> IsReady == XIL_COMPONENT_IS_READY);

    int i;

    if ((offset + length) > (XNN_AXILITES_ADDR_INPUT_R_HIGH - XNN_AXILITES_ADDR_INPUT_R_BASE + 1))
        return 0;

    for (i = 0; i < length; i++) {
        *(data + i) = *(char *)(InstancePtr->Axilites_BaseAddress + XNN_AXILITES_ADDR_INPUT_R_BASE + offset + i);
    }
    return length;
}

u32 XNn_Get_output_r_BaseAddress(XNn *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return (InstancePtr->Axilites_BaseAddress + XNN_AXILITES_ADDR_OUTPUT_R_BASE);
}

u32 XNn_Get_output_r_HighAddress(XNn *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return (InstancePtr->Axilites_BaseAddress + XNN_AXILITES_ADDR_OUTPUT_R_HIGH);
}

u32 XNn_Get_output_r_TotalBytes(XNn *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return (XNN_AXILITES_ADDR_OUTPUT_R_HIGH - XNN_AXILITES_ADDR_OUTPUT_R_BASE + 1);
}

u32 XNn_Get_output_r_BitWidth(XNn *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XNN_AXILITES_WIDTH_OUTPUT_R;
}

u32 XNn_Get_output_r_Depth(XNn *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XNN_AXILITES_DEPTH_OUTPUT_R;
}

u32 XNn_Write_output_r_Words(XNn *InstancePtr, int offset, int *data, int length) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr -> IsReady == XIL_COMPONENT_IS_READY);

    int i;

    if ((offset + length)*4 > (XNN_AXILITES_ADDR_OUTPUT_R_HIGH - XNN_AXILITES_ADDR_OUTPUT_R_BASE + 1))
        return 0;

    for (i = 0; i < length; i++) {
        *(int *)(InstancePtr->Axilites_BaseAddress + XNN_AXILITES_ADDR_OUTPUT_R_BASE + (offset + i)*4) = *(data + i);
    }
    return length;
}

u32 XNn_Read_output_r_Words(XNn *InstancePtr, int offset, int *data, int length) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr -> IsReady == XIL_COMPONENT_IS_READY);

    int i;

    if ((offset + length)*4 > (XNN_AXILITES_ADDR_OUTPUT_R_HIGH - XNN_AXILITES_ADDR_OUTPUT_R_BASE + 1))
        return 0;

    for (i = 0; i < length; i++) {
        *(data + i) = *(int *)(InstancePtr->Axilites_BaseAddress + XNN_AXILITES_ADDR_OUTPUT_R_BASE + (offset + i)*4);
    }
    return length;
}

u32 XNn_Write_output_r_Bytes(XNn *InstancePtr, int offset, char *data, int length) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr -> IsReady == XIL_COMPONENT_IS_READY);

    int i;

    if ((offset + length) > (XNN_AXILITES_ADDR_OUTPUT_R_HIGH - XNN_AXILITES_ADDR_OUTPUT_R_BASE + 1))
        return 0;

    for (i = 0; i < length; i++) {
        *(char *)(InstancePtr->Axilites_BaseAddress + XNN_AXILITES_ADDR_OUTPUT_R_BASE + offset + i) = *(data + i);
    }
    return length;
}

u32 XNn_Read_output_r_Bytes(XNn *InstancePtr, int offset, char *data, int length) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr -> IsReady == XIL_COMPONENT_IS_READY);

    int i;

    if ((offset + length) > (XNN_AXILITES_ADDR_OUTPUT_R_HIGH - XNN_AXILITES_ADDR_OUTPUT_R_BASE + 1))
        return 0;

    for (i = 0; i < length; i++) {
        *(data + i) = *(char *)(InstancePtr->Axilites_BaseAddress + XNN_AXILITES_ADDR_OUTPUT_R_BASE + offset + i);
    }
    return length;
}

void XNn_InterruptGlobalEnable(XNn *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XNn_WriteReg(InstancePtr->Axilites_BaseAddress, XNN_AXILITES_ADDR_GIE, 1);
}

void XNn_InterruptGlobalDisable(XNn *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XNn_WriteReg(InstancePtr->Axilites_BaseAddress, XNN_AXILITES_ADDR_GIE, 0);
}

void XNn_InterruptEnable(XNn *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XNn_ReadReg(InstancePtr->Axilites_BaseAddress, XNN_AXILITES_ADDR_IER);
    XNn_WriteReg(InstancePtr->Axilites_BaseAddress, XNN_AXILITES_ADDR_IER, Register | Mask);
}

void XNn_InterruptDisable(XNn *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XNn_ReadReg(InstancePtr->Axilites_BaseAddress, XNN_AXILITES_ADDR_IER);
    XNn_WriteReg(InstancePtr->Axilites_BaseAddress, XNN_AXILITES_ADDR_IER, Register & (~Mask));
}

void XNn_InterruptClear(XNn *InstancePtr, u32 Mask) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XNn_WriteReg(InstancePtr->Axilites_BaseAddress, XNN_AXILITES_ADDR_ISR, Mask);
}

u32 XNn_InterruptGetEnabled(XNn *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XNn_ReadReg(InstancePtr->Axilites_BaseAddress, XNN_AXILITES_ADDR_IER);
}

u32 XNn_InterruptGetStatus(XNn *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XNn_ReadReg(InstancePtr->Axilites_BaseAddress, XNN_AXILITES_ADDR_ISR);
}

