// ==============================================================
// File generated on Sun Aug 09 20:43:33 CEST 2020
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:36:41 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XNN_H
#define XNN_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xnn_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
#else
typedef struct {
    u16 DeviceId;
    u32 Axilites_BaseAddress;
} XNn_Config;
#endif

typedef struct {
    u32 Axilites_BaseAddress;
    u32 IsReady;
} XNn;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XNn_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XNn_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XNn_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XNn_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XNn_Initialize(XNn *InstancePtr, u16 DeviceId);
XNn_Config* XNn_LookupConfig(u16 DeviceId);
int XNn_CfgInitialize(XNn *InstancePtr, XNn_Config *ConfigPtr);
#else
int XNn_Initialize(XNn *InstancePtr, const char* InstanceName);
int XNn_Release(XNn *InstancePtr);
#endif

void XNn_Start(XNn *InstancePtr);
u32 XNn_IsDone(XNn *InstancePtr);
u32 XNn_IsIdle(XNn *InstancePtr);
u32 XNn_IsReady(XNn *InstancePtr);
void XNn_EnableAutoRestart(XNn *InstancePtr);
void XNn_DisableAutoRestart(XNn *InstancePtr);

u32 XNn_Get_input_r_BaseAddress(XNn *InstancePtr);
u32 XNn_Get_input_r_HighAddress(XNn *InstancePtr);
u32 XNn_Get_input_r_TotalBytes(XNn *InstancePtr);
u32 XNn_Get_input_r_BitWidth(XNn *InstancePtr);
u32 XNn_Get_input_r_Depth(XNn *InstancePtr);
u32 XNn_Write_input_r_Words(XNn *InstancePtr, int offset, int *data, int length);
u32 XNn_Read_input_r_Words(XNn *InstancePtr, int offset, int *data, int length);
u32 XNn_Write_input_r_Bytes(XNn *InstancePtr, int offset, char *data, int length);
u32 XNn_Read_input_r_Bytes(XNn *InstancePtr, int offset, char *data, int length);
u32 XNn_Get_output_r_BaseAddress(XNn *InstancePtr);
u32 XNn_Get_output_r_HighAddress(XNn *InstancePtr);
u32 XNn_Get_output_r_TotalBytes(XNn *InstancePtr);
u32 XNn_Get_output_r_BitWidth(XNn *InstancePtr);
u32 XNn_Get_output_r_Depth(XNn *InstancePtr);
u32 XNn_Write_output_r_Words(XNn *InstancePtr, int offset, int *data, int length);
u32 XNn_Read_output_r_Words(XNn *InstancePtr, int offset, int *data, int length);
u32 XNn_Write_output_r_Bytes(XNn *InstancePtr, int offset, char *data, int length);
u32 XNn_Read_output_r_Bytes(XNn *InstancePtr, int offset, char *data, int length);

void XNn_InterruptGlobalEnable(XNn *InstancePtr);
void XNn_InterruptGlobalDisable(XNn *InstancePtr);
void XNn_InterruptEnable(XNn *InstancePtr, u32 Mask);
void XNn_InterruptDisable(XNn *InstancePtr, u32 Mask);
void XNn_InterruptClear(XNn *InstancePtr, u32 Mask);
u32 XNn_InterruptGetEnabled(XNn *InstancePtr);
u32 XNn_InterruptGetStatus(XNn *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
