// ==============================================================
// File generated on Sun Aug 09 20:43:33 CEST 2020
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:36:41 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
// AXILiteS
// 0x000 : Control signals
//         bit 0  - ap_start (Read/Write/COH)
//         bit 1  - ap_done (Read/COR)
//         bit 2  - ap_idle (Read)
//         bit 3  - ap_ready (Read)
//         bit 7  - auto_restart (Read/Write)
//         others - reserved
// 0x004 : Global Interrupt Enable Register
//         bit 0  - Global Interrupt Enable (Read/Write)
//         others - reserved
// 0x008 : IP Interrupt Enable Register (Read/Write)
//         bit 0  - Channel 0 (ap_done)
//         bit 1  - Channel 1 (ap_ready)
//         others - reserved
// 0x00c : IP Interrupt Status Register (Read/TOW)
//         bit 0  - Channel 0 (ap_done)
//         bit 1  - Channel 1 (ap_ready)
//         others - reserved
// 0x400 ~
// 0x7ff : Memory 'input_r' (784 * 8b)
//         Word n : bit [ 7: 0] - input_r[4n]
//                  bit [15: 8] - input_r[4n+1]
//                  bit [23:16] - input_r[4n+2]
//                  bit [31:24] - input_r[4n+3]
// 0x800 ~
// 0x83f : Memory 'output_r' (10 * 32b)
//         Word n : bit [31:0] - output_r[n]
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

#define XNN_AXILITES_ADDR_AP_CTRL       0x000
#define XNN_AXILITES_ADDR_GIE           0x004
#define XNN_AXILITES_ADDR_IER           0x008
#define XNN_AXILITES_ADDR_ISR           0x00c
#define XNN_AXILITES_ADDR_INPUT_R_BASE  0x400
#define XNN_AXILITES_ADDR_INPUT_R_HIGH  0x7ff
#define XNN_AXILITES_WIDTH_INPUT_R      8
#define XNN_AXILITES_DEPTH_INPUT_R      784
#define XNN_AXILITES_ADDR_OUTPUT_R_BASE 0x800
#define XNN_AXILITES_ADDR_OUTPUT_R_HIGH 0x83f
#define XNN_AXILITES_WIDTH_OUTPUT_R     32
#define XNN_AXILITES_DEPTH_OUTPUT_R     10

