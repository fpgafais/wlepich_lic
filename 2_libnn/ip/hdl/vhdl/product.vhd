-- ==============================================================
-- RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
-- Version: 2018.3
-- Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
-- 
-- ===========================================================

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity product is
port (
    ap_ready : OUT STD_LOGIC;
    a_V : IN STD_LOGIC_VECTOR (7 downto 0);
    w_V : IN STD_LOGIC_VECTOR (10 downto 0);
    ap_return : OUT STD_LOGIC_VECTOR (11 downto 0) );
end;


architecture behav of product is 
    constant ap_const_logic_1 : STD_LOGIC := '1';
    constant ap_const_boolean_1 : BOOLEAN := true;
    constant ap_const_lv32_7 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000000111";
    constant ap_const_lv32_12 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000010010";
    constant ap_const_logic_0 : STD_LOGIC := '0';

    signal r_V_2_fu_43_p2 : STD_LOGIC_VECTOR (18 downto 0);
    signal r_V_2_fu_43_p1 : STD_LOGIC_VECTOR (7 downto 0);
    signal r_V_2_fu_43_p10 : STD_LOGIC_VECTOR (18 downto 0);

    component nn_mul_mul_11s_8ns_19_1_1 IS
    generic (
        ID : INTEGER;
        NUM_STAGE : INTEGER;
        din0_WIDTH : INTEGER;
        din1_WIDTH : INTEGER;
        dout_WIDTH : INTEGER );
    port (
        din0 : IN STD_LOGIC_VECTOR (10 downto 0);
        din1 : IN STD_LOGIC_VECTOR (7 downto 0);
        dout : OUT STD_LOGIC_VECTOR (18 downto 0) );
    end component;



begin
    nn_mul_mul_11s_8ns_19_1_1_U3 : component nn_mul_mul_11s_8ns_19_1_1
    generic map (
        ID => 1,
        NUM_STAGE => 1,
        din0_WIDTH => 11,
        din1_WIDTH => 8,
        dout_WIDTH => 19)
    port map (
        din0 => w_V,
        din1 => r_V_2_fu_43_p1,
        dout => r_V_2_fu_43_p2);




    ap_ready <= ap_const_logic_1;
    ap_return <= r_V_2_fu_43_p2(18 downto 7);
    r_V_2_fu_43_p1 <= r_V_2_fu_43_p10(8 - 1 downto 0);
    r_V_2_fu_43_p10 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(a_V),19));
end behav;
