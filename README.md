# NN-Revision

Rozpoznawanie cyfr przez sieć neuronową na FPGA krok po kroku.

## Oprogramowanie

Do stworzenia projektu użyto:

- Vivado HLS 2018.3
- Vivado SDx 2018.3
- hls4ml 0.2.0
- TensorFlow 2.1.0

## Z Pythona do C++

W folderze `1_hls4ml` znajduje się projekt Vivado HLS, w którym jest przetłumaczona przez hls4ml sieć.

Plik `hls4ml_config.yml` nie zawiera ostatecznej konfiguracji modelu -
ta została dopasowana bezpośrednio w pliku `firmware/defines.h`.

W Vivado HLS zsyntetyzowaną sieć należy eksportować opcją *Export RTL*.
Powstały folder `nn_prj/<solution>/impl/ip` będzie potrzebny w następnym kroku.

## Biblioteka C-callable

Krok drugi - `2_libnn` zawiera folder `ip` oraz plik nagłówkowy
`nn.hpp`, które są potrzebne do stworzenia biblioteki wykorzystywanej
w następnych etapach projektu.

Do stworzenia biblioteki wykorzystane jest narzędzie `sdx_pack`.
Przygotowane polecenie jest w skrypcie `create_library.sh` - uruchomienie
go stworzy bibliotekę statyczną `libnn.a`.

## neuralnet

Do zbudowania tej biblioteki potrzebna jest platforma reVISION dostępna w
[repozytorium na GitHubie](https://github.com/Xilinx/Embedded-Reference-Platforms-User-Guide/blob/2018.3/Docs/software-tools-system-requirements.md#32-software)
W projekcie użyta została platforma *Single-sensor ZCU104 production silicon*.

W folderze `3_neuralnet` jest folder `neuralnet` - projekt SDSoC z plikami
źródłowymi w folderze `src`.

Ze względu na sposób zapisu projektu, bezpośredni import może nie
zadziałać - w pliku `project.sdx` (wewnętrznie jest to .xml) są dwa
parametry: *location* i *platform*, których wartościami są ścieżki do
kolejno folderu-workspace oraz do pliku określającego platformę.
Można albo utworzyć nowy projekt (SDx Library Project - Shared Library)
i skopiować pliki źródłowe albo zmienić odpowiednio te dwa parametry.

W przypadku utworzenia nowego projektu należy:

1. skopiować pliki źródłowe (`net_sds.cpp`, `net_sds.h` i `nn.hpp`)
2. dodać funkcje hardwarowe: `read_net_input` i `write_net_output`,
3. włączyć bibliotekę `nn` do projektu (*C/C++ Build -> Settings -> SDS++ Linker -> Libraries*)

W opcjach projektu ustawiono opcję *Data motion network clock frequency* na
75MHz - na pewnym etapie projektu próbowałem wielu różnych rozwiązań
jakiegoś problemu z zegarami, niestety nie pamiętam na czym on polegał
i możliwe że nie jest to konieczne.

### Ważne kroki

W opcjach budowania (*C/C++ Build -> Settings -> SDS++ Linker*) jako
polecenie użyto:
`sds++ -xp "vivado_prop:run.impl_1.{STEPS.PLACE_DESIGN.ARGS.MORE OPTIONS}={-fanout_opt}"` - dodatkowa konfiguracja syntezy.

W zakładce *SDS++ Compiler -> Inferred Options -> Software Platform*
dopisano w polu *Software Platform Inferred Flags*: "-hls-target 1"
(bez cudzysłowu).

Po zbudowaniu projektu wygenerowane są pliki (w folderze `sd_card`) w tym biblioteka, które trzeba skopiować na kartę pamięci (nie są to jedyne pliki na kartę).

## gstsdxnet

Pliki źródłowe znajdują się w folderze `4_gstsdxnet`. Należy utworzyć nowy
projekt-bibliotekę (jak w `neuralnet`), skopiować pliki źródłowe do folderu
`src`.

Ustawienia projektu:

1. Dodać zmienną środowiskową (*C/C++ Build -> Environment*): SYSROOT z
   wartością: (ścieżka do platformy)/zcu104-rv-ss-2018-3/zcu104_rv_ss/sw/a53_linux/a53_linux/sysroot/aarch64-xilinx-linux
2. Łańcuch narzędzi (*C/C++ Build -> Tool Chain Editor*):
   - odznaczyć "Display compatible toolchains only"
   - wybrać "Xilinx ARM v8 GNU/Linux Toolchain"
3. *C/C++ Build -> Settings*:
    - *g++ compiler*:
      - Optimiation: Optimize More (-O2)
      - Directories - dodać następujące ścieżki:
        - ścieżka do folderu `src` z neuralnet
        - ${SYSROOT}/usr/include
        - ${SYSROOT}/usr/include/glib-2.0
        - ${SYSROOT}/usr/lib/glib-2.0/include
        - ${SYSROOT}/usr/include/gstreamer-1.0
        - ${SYSROOT}/usr/lib/gstreamer-1.0/include
      - Miscellaneous: - w polu *Other flags* wpisać: "-c -fPIC -fpermissive -fmessage-length=0 -MT"$@" --sysroot=${SYSROOT}"
    - *g++ linker*:
      - Libraries: dodać następując biblioteki:
        - neuralnet (nazwa zależna od nadanej nazwy krok wcześniej)
        - gstbase-1.0
        - gstvideo-1.0
        - gstallocators-1.0
        - gstsdxbase-1.0
        - gstsdxallocator-1.0
      - Dodać również w *Library search path*:
        - ścieżkę do folderu z biblioteką neuralnet
        - ${SYSROOT}/usr/lib
      - Miscellaneous: w polu *Linker flags*: "-shared --sysroot=${SYSROOT} -L=/lib -L=/usr/lib -Wl,-rpath-link=${SYSROOT}/lib,-rpath-link=${SYSROOT}/usr/lib"

Po zbudowaniu projektu otrzymujemy bibliotekę `libgstsdxnet.so`, którą należy
skopiować na kartę pamięci.

## GStreamer pipeline

Strumień zdefiniowany jest jako ciąg argumentów programu `gst-launch-1.0`
rozdzielonych znakiem wykrzyknika. Przygotowany strumień jest w skrypcie
`run.sh` w folderze `5_pipeline`. Ten plik należy skopiować na kartę SD.

Parametry są przygotowane dla wyświetlania obrazu przez HDMI - w przypadku
wyświetlania na DisplayPort należy zmienić wartość zmiennej `plane` na 34
(nietestowane).

## Uruchamianie

Instrukcje przygotowania płyty i uruchomienia dostępne są w [repozytorium reVISION](https://github.com/Xilinx/Embedded-Reference-Platforms-User-Guide/blob/2018.3/Docs/operating-instructions.md).

Po uruchomieniu należy przejść do folderu `/media/card` i uruchomić skrypt
`run.sh`.
